# How to use this repo #

## Initial setup ##

### Clone

    git clone ssh://git@bitbucket.org/sthaha/coursera-hw-sw-interface.git

### create your branch

    git push  origin HEAD:refs/heads/<your-name>`


### checkout your branch

    git checkout <your-name>


## Sharing your changes ##
   - Make sure you have checkout your branch

        git branch      // should say <your-name>

   - Add your files

          git add <your-local-files>

   - Push it to remote

          git push

